#python reverstring without using reverse - recursive  method

def reversestring(text):
	if len(text)==0:
		return text
	else:
		return reversestring(text[1:])+text[0]
print (reversestring("Manjaro"))